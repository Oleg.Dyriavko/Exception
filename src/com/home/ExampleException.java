package com.home;

public class ExampleException extends Exception {
    private String myText;

    /**
     * Constructs a new exception with {@code null} as its detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     */
    public ExampleException(String myText) {
        this.myText = myText;
    }

    public String getMyText() {
        return myText;
    }
}
