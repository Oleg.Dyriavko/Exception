package com.home;

public class Main {

    public static void main(String[] args){ // throws Exception - 1 метод обработки исключения

        Example example = new Example();

        // 2 метод обработки исключения - блок try/catch
        try {
        example.method1();
        example.method2();
        }catch (ExampleException e){
            System.out.println(e.toString());
        }
        catch (NewException e){
            System.out.println(e.toString());
        }


    }
}
